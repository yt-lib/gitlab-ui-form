const path = require('path')

const myProcess = async ({ name }) => {
	if (path.basename(name).startsWith('main.') && name.endsWith('.js')) {
		return {
			header: `!(function(parcelRequire){`,
			footer: `})();`
		}
	}
}

module.exports = myProcess
