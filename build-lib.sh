set -Ceu

yarn rimraf dist

parcel build src/main.js src/style.scss --no-cache --no-source-maps
