import { parse, stringify } from './compact'

const testToken = {
	type: 'demotoken',
	title: 'Unique',
	icon: 'document',
	token: 'gl-filtered-search-token',
	operators: [{ value: '=', description: 'is', default: 'true' }],
	options: [
		{ icon: 'heart', title: 'heart', value: 1 },
		{ icon: 'hook', title: 'hook', value: 2 },
	],
	unique: true,
}

const testToken2 = {
	type: 'demotoken2',
	title: 'Name',
	icon: 'document',
	token: 'gl-filtered-search-token',
	operators: [
		{ value: 'includes', _description: 'is', default: 'true' },
		{ value: 'ex', _description: 'ex' },
	],
	// options: [
	// 	{ icon: 'heart', title: 'heart', value: 1 },
	// 	{ icon: 'hook', title: 'hook', value: 2 },
	// ],
	// unique: true,
}

const range = {
	operators: [
		{ value: '≧', description: '以上', suffix: 'from' },
		{ value: '≦', description: '以下', suffix: 'to' },
	],
	uniqueWithOperator: true,
}

export default {
	name: 'App',
	data() {
		const url = new URL(location.href)
		const value = parse(url.searchParams.get('q'))
		let v
		if ((v = url.searchParams.get('demotoken2'))) {
			value.push({ type: 'demotoken2', value: { data: v, operator: '=' } })
		}
		return {
			tokens: {
				[testToken.type]: testToken,
				[testToken2.type]: testToken2,
				label: { icon: 'labels', title: 'Label', token: 'en-label-token' },
				user: { icon: 'user', title: 'User', token: 'en-user-token' },
				birth: { icon: 'labels', title: 'birthdate', token: 'en-date-token' },
				due: {
					icon: 'labels',
					title: 'Due',
					token: 'en-date-token',
					...range,
				},
			},
			value,
		}
	},
	methods: {
		handleSubmit(tokens) {
			const s = stringify(tokens)
			alert(s)
			const url = new URL(location.href)
			url.search = ''
			url.searchParams.set('q', s)
			location.href = String(url)
		},
	},
	render(h) {
		return h('div', {}, [
			h('p', {}, ['hogehoge']),
			h('div', {}, [
				h('glui-form', {
					props: { tokens: this.tokens },
					model: {
						value: this.value,
						callback: l => {
							this.value = l
						},
						expression: 'value',
					},
					on: {
						submit: this.handleSubmit,
					},
				}),
			]),
			h('div', {}, [
				h('pre', {}, [h('code', {}, [JSON.stringify(this.value, null, 2)])]),
			]),
		])
	},
}
