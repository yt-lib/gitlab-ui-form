/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-ts-comment */
import Vue from 'vue'
import type { PropType } from 'vue'
import { fixTokens } from './fixTokens'
import type { SearchToken, SmallToken, ValueToken } from './fixTokens'

export default Vue.extend({
	name: 'GluiForm',
	data: (): Record<never, never> => ({}),
	props: {
		tokens: {
			type: Object as PropType<Record<string, SmallToken>>,
			required: true,
		},
		value: { type: Array as PropType<ValueToken[]>, required: true },
	},
	computed: {
		tokenList(): SearchToken[] {
			return fixTokens(this.tokens)
		},
		availableTokens(): SearchToken[] {
			return this.tokenList.map(t => {
				if (t.disabled || !t.operators || !t.uniqueWithOperator) return t
				const values = this.value.filter(v => v.type === t.type)
				const disabled =
					new Set(values.map(v => v.value && v.value.operator)).size >=
					t.operators.length
				const operators = t.operators.map(o => {
					const disabled = values.some(
						v => v.value && o.value === v.value.operator,
					)
					return { ...o, disabled }
				})
				return { ...t, operators, disabled }
			})
		},
	},
	methods: {
		handleSubmit(tokens: ValueToken[]) {
			this.$emit('submit', tokens)
		},
		handleChange(tokens: ValueToken[]) {
			this.$emit('change', tokens)
		},
	},
	render(h) {
		return h(
			'gl-filtered-search',
			{
				// @ts-ignore
				attrs: { 'available-tokens': this.availableTokens },
				// @ts-ignore
				model: {
					value: this.value,
					// @ts-ignore
					callback: this.handleChange,
					expression: 'value',
				},
				on: {
					// @ts-ignore
					submit: this.handleSubmit,
				},
			},
			undefined,
		)
	},
})
