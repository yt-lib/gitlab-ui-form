import { stringify } from './compact2'
import GluiForm from './GluiForm'
import { fixTokens } from './fixTokens'

const fixValue = v => {
	if (!Array.isArray(v)) return []
	return v.map(v => {
		if (v.value && v.value.data && v.value.data.includes(' '))
			return { ...v, value: { ...v.value, data: '"' + v.value.data + '"' } }
		return v
	})
}

export const SearchForm = {
	name: 'SearchForm',
	props: {
		tokens: {
			type: Object,
			required: true,
		},
		value: {
			type: Array,
			required: true,
		},
	},
	data() {
		return {
			currentValue: fixValue(this.value),
		}
	},
	computed: {
		availableTokens() {
			return this.tokens
		},
	},
	methods: {
		push(q) {
			location.search = stringify(q, fixTokens(this.availableTokens))
		},
	},
	render(h) {
		return h(GluiForm, {
			attrs: { tokens: this.availableTokens },
			on: { submit: this.push },
			model: {
				value: this.currentValue,
				callback: l => {
					this.currentValue = l
				},
				expression: 'value',
			},
		})
	},
}

export default SearchForm
