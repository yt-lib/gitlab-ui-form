export type Token = {
	type: string
	value: {
		data: string
		operator?: string
	}
}

// type _Data = [string, string] | [string, string, string]

export const parse = (data: string): Token[] =>
	(data || '')
		.split(/;/gu)
		.filter(Boolean)
		.map(part => {
			const data = part.split(/!/gu).map(unescape)
			return {
				type: data[0],
				value: {
					data: data[1],
					operator: data[2],
				},
			}
		})

export const stringify = (tokens: Token[]): string =>
	tokens
		.map(t => {
			const p = 'string' === typeof t ? ['_', t] : [t.type, t.value.data]
			if (null != t.value?.operator) p[2] = t.value.operator
			return p.map(escape).join('!')
		})
		.join(';')

export const escape = (v: string | number) =>
	null == v
		? ''
		: String(v).replace(/\//gu, '//').replace(/!/gu, '/e').replace(/;/gu, '/s')

export const unescape = (v: string) =>
	v.replace(/\/s/gu, ';').replace(/\/e/gu, '!').replace(/\/\//gu, '/')
