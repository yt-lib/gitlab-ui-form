export type VToken = {
	type: string
	value: {
		data: string
		operator?: string
	}
}

export type SToken = {
	type: string
	operators: {
		value: string
		suffix?: string
		key?: string
	}[]
	unique?: boolean
	uniqueWithOperator?: boolean
	defaultKey?: boolean
	readKey?: boolean
}

const unwrap = (data: string) =>
	data.startsWith('"') && data.endsWith('"') && data.includes(' ')
		? data.slice(1, -1)
		: data

export const stringify = (vtokens: VToken[], stokens: SToken[]): string => {
	const q = new URLSearchParams()
	for (const st of stokens) {
		const isUnq = st.unique || st.uniqueWithOperator
		for (const t of vtokens) {
			if ('string' === typeof t && st.defaultKey) {
				q.append(st.type, t)
			} else if (t.type === st.type) {
				for (const op of st.operators) {
					if (op.value === t.value.operator) {
						const key = op.key || st.type + (op.suffix ? '_' + op.suffix : '')
						if (st.readKey) {
							q.append(key + ('[' + unwrap(t.value.data) + ']'), 'on')
						} else {
							q.append(key + (isUnq ? '' : '[]'), unwrap(t.value.data))
						}
					}
				}
			}
		}
	}

	const map: (v: URLSearchParams) => URLSearchParams =
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		(window as any)['en:gitlab:ui:form:stringify'] || (a => a)
	return map(q).toString()
}
