import type { Component, AsyncComponent } from 'vue'

export type SearchToken = {
	title: string
	operators: {
		value: string
		description?: string
		default?: string
	}[]
	token: string | Component | AsyncComponent
	options?: {
		icon?: string
		id?: string | number
		title: string
		value: string
	}[]
	unique?: true
	disabled?: boolean
	uniqueWithOperator?: true
	type: string
	icon?: string
}

export type ValueToken = {
	type: string
	value: {
		data: string
		operator?: string
	}
}

type OptField = 'operators' | 'token'

export type SmallToken = Omit<SearchToken, 'type' | OptField> &
	Partial<Pick<SearchToken, OptField>>

export type ApiTokenInfo =
	| {
			'en:is': 'info'
			pathname: string
			titlekey: string
			resultskey: string
	  }
	| {
			'en:is': 'func'
			code: string
	  }
