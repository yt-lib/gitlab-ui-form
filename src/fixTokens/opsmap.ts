export const opsmap = {
	def: [{ value: '含む', default: 'true' }],
	eq: [{ value: '=', description: '等しい', default: 'true' }],
	has: [{ value: 'has', description: '所持', default: 'true' }],
	range: [
		{ value: '≧', description: '以上' },
		{ value: '≦', description: '以下' },
	],
	date: [
		{ value: '≧', description: '以降' },
		{ value: '≦', description: '以前' },
	],
}
