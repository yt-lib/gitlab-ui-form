const main = document.querySelector('main')
const div = document.createElement('div')
div.setAttribute('data-need-dump', '[data-dump]')
/** @type {[string, string][]} */
const params = Array.from(new URL(location.href).searchParams)
const value = params.map(([_k, data]) => {
	const type = _k.replace(/\[\]$/giu, '')
	const operator = '含む'
	return { type, value: { data, operator } }
})
div.setAttribute(
	'data-is-search-form',
	JSON.stringify({
		tokens: {
			text: {
				title: 'text',
				icon: 'labels',
				operators: [{ value: '含む', default: 'true' }],
				token: 'gl-filtered-search-token',
			},
			user: {
				title: 'user',
				token: {
					'en:is': 'func',
					code: `
						let body
						if (word)
							body = JSON.stringify({
								query: \`query($word: String) {
									users(search: $word, first: 25) {
										nodes { id title: name value: username }
									}
								}\`,
								variables: { word }
							})
						if (value)
							body = JSON.stringify({
								query: \`query($value: [String!]) {
									users(usernames: $value, first: 25) {
										nodes { id title: name value: username }
									}
								}\`,
								variables: { value: [value] }
							})
						return fetch('https://gitlab.com/api/graphql', {
							method: 'post',
							headers: { 'content-type': 'application/json' },
							body,
						})
							.then(r => r.json())
							.then(d => d.data.users.nodes)
					`,
				},
			},
		},
		value,
	}),
)
main.appendChild(div)
