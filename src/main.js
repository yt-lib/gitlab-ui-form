import 'core-js'
import Vue from 'vue'

import GlToken from '@gitlab/ui/dist/components/base/token/token'
import GlFilteredSearch from '@gitlab/ui/dist/components/base/filtered_search/filtered_search'
import GlFilteredSearchToken from '@gitlab/ui/dist/components/base/filtered_search/filtered_search_token'
import GlFilteredSearchSuggestion from '@gitlab/ui/dist/components/base/filtered_search/filtered_search_suggestion'
import GlFilteredSearchTokenSegment from '@gitlab/ui/dist/components/base/filtered_search/filtered_search_token_segment'
import GlDropdownDivider from '@gitlab/ui/dist/components/base/dropdown/dropdown_divider'
import GlLoadingIcon from '@gitlab/ui/dist/components/base/loading_icon/loading_icon'

import EnDateTimeToken from './tokens/DateTimeToken.vue'
import EnDateToken from './tokens/DateToken.vue'
import EnIntToken from './tokens/IntToken.vue'

import SearchForm from './SearchForm'

{
	const origFn = GlFilteredSearchTokenSegment.methods.handleBlur
	GlFilteredSearchTokenSegment.methods.handleBlur = function handleBlur() {
		if (this.$refs.input.hasAttribute('data-js-stop')) return
		return origFn.apply(this, arguments)
	}
}
{
	const origFn = GlFilteredSearchTokenSegment.methods.handleInputKeydown
	GlFilteredSearchTokenSegment.methods.handleInputKeydown = function handleInputKeydown() {
		const e = arguments[0]
		if (e.isComposing) return
		return origFn.apply(this, arguments)
	}
}
{
	const origFn = GlFilteredSearchTokenSegment.render
	GlFilteredSearchTokenSegment.render = function render() {
		const self = new Proxy(this, {
			get: (targat, key) => {
				if ('options' !== key) return targat[key]
				if (!targat.options) return targat.options
				return targat.options.filter(o => !o.disabled)
			},
		})
		return origFn.apply(self, arguments)
	}
}

Vue.component('GlFilteredSearch', GlFilteredSearch)
Vue.component('GlFilteredSearchToken', GlFilteredSearchToken)
Vue.component('GlToken', GlToken)
Vue.component('GlFilteredSearchSuggestion', GlFilteredSearchSuggestion)
Vue.component('GlDropdownDivider', GlDropdownDivider)
Vue.component('GlLoadingIcon', GlLoadingIcon)

Vue.component('EnDateTimeToken', EnDateTimeToken)
Vue.component('EnDateToken', EnDateToken)
Vue.component('EnIntToken', EnIntToken)

const mountmap = [{ attr: 'data-is-search-form', Form: SearchForm }]

const cssready = (() => {
	let p = false
	const init = () => {
		const e = document.querySelector('[data-search-form-style-src]')
		const s = e?.getAttribute('data-search-form-style-src') || './style.css'
		return fetch(s).then(r => r.text())
	}
	return () => p || (p = init())
})()

const mount = () => {
	const list = []
	const parseAttr = attr => {
		try {
			if (attr) return JSON.parse(attr)
		} catch (x) {
			// pass
		}
	}
	for (const { attr, Form } of mountmap) {
		const needdump = document
			.querySelector('[data-need-dump]')
			?.getAttribute('data-need-dump')
		const r = needdump && document.querySelector(needdump)
		if (r) {
			Form.watch = {
				currentValue: {
					handler: e => {
						r.innerText = JSON.stringify(e, null, 2)
					},
					deep: true,
				},
			}
		}
		for (const main of document.querySelectorAll(`[${attr}]`)) {
			list.push(
				cssready().then(styles => {
					main.innerHTML = ''
					const props = { ...parseAttr(main.getAttribute(attr)) }
					main.attachShadow({ mode: 'open' })
					const wrap = document.createElement('div')
					const style = document.createElement('style')
					style.innerText = styles
					wrap.appendChild(style)
					main.shadowRoot.appendChild(wrap)
					const el = document.createElement('div')
					wrap.appendChild(el)
					new Vue({ el, render: h => h(Form, { props }) })
				}),
			)
		}
	}
	return Promise.all(list)
}

mount().catch(x => {
	console.error(x)
})
